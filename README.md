# online-voting-system
CS6905 Project

This is an online voting system or e-voting system. An election can be at different levels like broad region (Parliament) or in a small community (Party) as required. In this system there are three type of user; Admin (Election Community), Candidate and Party and Voter (Any person, Admin and Candidate). General registration is required for any activity.

Admin is in charge of system maintenance, Create new election or update existing election, Candidate and Voter registration Verification, Notice Upload, set rules and regulation for the election and also able to block Candidate or Voter for any rules violations. 

Candidate and voter can see notice, election result those uploads by admin. Also everyone can edit there profile, cast vote also can change their previse vote. 

Candidate or Party needs a registration to participant in election. So candidate need to register for election as a nominee in the time frame set by admin.
